const  showHide = (() => {
    const data = document.getElementById ('navigation');
    const menu = document.getElementById ('menu');
    const cross = document.getElementById ('close');
    if (data.style.display === 'block') {
        data.style.display = 'none';
        menu.style.display = 'block';
        cross.style.display = 'none';
    }else {
        data.style.display = 'block';
        menu.style.display = 'none';
        cross.style.display = 'block';
    }
})